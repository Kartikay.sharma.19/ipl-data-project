// Number of matches played per year for all the years in IPL.
const matchesPlayedPerYear = (matchdata) => {

    let getMatchesPlayedPerYear = (matchesPerYear, currentMatch) => {
        if (matchesPerYear.hasOwnProperty(currentMatch.season)) {
            matchesPerYear[currentMatch.season] += 1;
        } else {
            matchesPerYear[currentMatch.season] = 1;
        }

        return matchesPerYear;
    }
    return matchdata.reduce(getMatchesPlayedPerYear, {});
}

// Number of matches won per team per year in IPL.
const teamWinCountPerYear = (matchdata) => {

    let getTeamWinCountPerYear = (winCountPerYear, currentMatch) => {
        if (winCountPerYear.hasOwnProperty(currentMatch.season)) {
            if (winCountPerYear[currentMatch.season].hasOwnProperty(currentMatch.winner)) {
                winCountPerYear[currentMatch.season][currentMatch.winner] += 1;
            }
            else if (currentMatch.winner != '') {
                winCountPerYear[currentMatch.season][currentMatch.winner] = 1;
            }
        } else {
            winCountPerYear[currentMatch.season] = {};
            winCountPerYear[currentMatch.season][currentMatch.winner] = 1;
        }
        return winCountPerYear;
    }

    return matchdata.reduce(getTeamWinCountPerYear, {});
}

// Extra runs conceded per team in the year 2016
const extraRunsConcededPerTeam = (matchdata, deliveriedata) => {
    const objOfYear = matchdata.filter(match => match.season == 2016);
    const idOfobjOfYear = objOfYear.map(match => match.id);

    let getextraRunsConcededPerTeam = (runsConceded, currentMatch) => {
        if (idOfobjOfYear.includes(currentMatch.match_id)) {
            if (runsConceded.hasOwnProperty(currentMatch.bowling_team)) {
                runsConceded[currentMatch.bowling_team] += parseInt(currentMatch.extra_runs);
            } else {
                runsConceded[currentMatch.bowling_team] = parseInt(currentMatch.extra_runs);
            }
        }
        return runsConceded;
    }

    return deliveriedata.reduce(getextraRunsConcededPerTeam, {});
}

// Top 10 economical bowlers in the year 2015
const economicalBowlersOfYear = (matchdata, deliveriedata) => {
    const objOfYear = matchdata.filter(match => match.season == 2015);
    const idOfobjOfYear = objOfYear.map(match => match.id);

    let getEconomicalBowlersOfYear = (bowlerEconomi, currentMatch) => {
        if (idOfobjOfYear.includes(currentMatch.match_id)) {
            if (bowlerEconomi.hasOwnProperty(currentMatch.bowler)) {
                let balls = bowlerEconomi[currentMatch.bowler].ball += 1;
                let totalRuns = bowlerEconomi[currentMatch.bowler].total_runs += parseInt(currentMatch.total_runs);
                bowlerEconomi[currentMatch.bowler].economi = totalRuns / (balls / 6);
            } else {
                bowlerEconomi[currentMatch.bowler] = {};
                let balls = bowlerEconomi[currentMatch.bowler].ball = 1;
                let totalRuns = bowlerEconomi[currentMatch.bowler].total_runs = parseInt(currentMatch.total_runs);
                bowlerEconomi[currentMatch.bowler].economi = totalRuns / (balls / 6);
            }
        }
        return bowlerEconomi;
    }
    const economiData = deliveriedata.reduce(getEconomicalBowlersOfYear, {});

    // arr that contains only economi from reduced deliveriedata
    let economiDataArray = []
    for (let property in economiData) {
        economiDataArray.push(economiData[property].economi);
    }

    //sort it in acending order
    economiDataArray.sort(function (a, b) {
        return a - b;
    });

    // gets top 10 economi bowlers info 
    let topTeneconomiBowlersOfYear = {};
    for (let index = 0; index < 10; index++) {
        for (let propertie in economiData) {
            if (economiData[propertie].economi == economiDataArray[index]) {
                topTeneconomiBowlersOfYear[propertie] = economiData[propertie].economi;
            }
        }
    }

    return topTeneconomiBowlersOfYear;
}



module.exports = {
    matchesPlayedPerYear,
    teamWinCountPerYear,
    extraRunsConcededPerTeam,
    economicalBowlersOfYear
    
};